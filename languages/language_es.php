<?php
	$texts = array(
		'Sign in' => 'Inicia sesión',
		'Username' => 'Usuario',
		'Password' => 'Contraseña',
		'Remember account' => 'Recordar cuenta',
		'Forgot your password?' => '¿Has olvidado la contraseña?',
		'If you dont remember your password, write your email address and we will send you a e-mail with the information to recover it.' => 'Si no recuerdas tu contraseña, escribe tu correo electrónico y te enviaremos un mensaje con las instrucciones para poder cambiar la contraseña.',
		'Send' => 'Enviar',
		'Cancel' => 'Cancelar',
		'Reset your password' => 'Restaura tu contraseña'
	);
?>
