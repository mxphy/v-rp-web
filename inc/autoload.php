<?php
include_once 'inc/functions.php';
include_once 'models/Language.php';
include_once 'models/Database.php';
include_once 'models/User.php';
include_once 'models/Forgot.php';
include_once 'models/csrf.php';
include_once 'controllers/UserController.php';
include_once 'controllers/ForgotController.php';

$lg = new Language();

$lang = null;
if (isset($_GET['lang']) ){
  $lang = $_GET['lang'];
}
 ?>
