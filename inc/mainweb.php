<?php

include_once 'inc/autoload.php';

$username = $_SESSION['vrpuser'];
$userInfo = User::getUserInfo($username);
foreach($userInfo as $row):
$socialclub = $row['SocialClubName'];
endforeach;

$navicons = sprintf('<div class="kt-header__topbar-item" title="IP del servidor ()" data-toggle="kt-tooltip" data-placement="top">
    <div class="kt-header__topbar-wrapper">
      <a href="samp://">
        <span class="kt-header__topbar-icon kt-header__topbar-icon--info">
          <i class="fas fa-gamepad"></i>
        </span>
      </a>
    </div>
  </div>

  <div class="kt-header__topbar-item" title="IP del TS ()" data-toggle="kt-tooltip" data-placement="top">
    <div class="kt-header__topbar-wrapper">
      <a href="ts3server://">
        <span class="kt-header__topbar-icon kt-header__topbar-icon--info">
          <i class="socicon-teamspeak"></i>
        </span>
      </a>
    </div>
  </div>

  <div class="kt-header__topbar-item" title="Discord" data-toggle="kt-tooltip" data-placement="top">
    <div class="kt-header__topbar-wrapper">
      <a href="">
        <span class="kt-header__topbar-icon kt-header__topbar-icon--info">
        <i class="socicon-discord"></i>
        </span>
      </a>
    </div>
  </div>');

 $navmenu = sprintf('<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
   <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
     <ul class="kt-menu__nav ">
       <li class="kt-menu__item  kt-menu__item--open kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open " data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="lelas" class="kt-menu__link"><span class="kt-menu__link-text">Inicio</span></a></li>
       <li class="kt-menu__item  kt-menu__item--open kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open " data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="lelas" class="kt-menu__link"><span class="kt-menu__link-text">Normativa</span></a></li>
       <li class="kt-menu__item  kt-menu__item--open kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open " data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="lelas" class="kt-menu__link"><span class="kt-menu__link-text">Estado</span></a></li>
       <li class="kt-menu__item  kt-menu__item--open kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open " data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="lelas" class="kt-menu__link"><span class="kt-menu__link-text">Soporte</span></a></li>
     </ul>
   </div>
   <div class="kt-header-toolbar">
     <div class="kt-quick-search" id="kt_quick_search_default">
       <a href="" class="btn btn-label-info btn-small btn-block">IR AL FORO</a>
     </div>
   </div>
 </div>');

 $footerweb = sprintf('<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer">
 						<div class="kt-footer__top">
 							<div class="kt-container">
 								<div class="row">
 									<div class="col-lg-4">
 										<div class="kt-footer__section">
 											<h3 class="kt-footer__title">Sobre </h3>
 											<div class="kt-footer__content">

 											</div>
 										</div>
 									</div>
 									<div class="col-lg-4">
 										<div class="kt-footer__section">
 											<h3 class="kt-footer__title">Quick Links</h3>
 											<div class="kt-footer__content">
 												<div class="kt-footer__nav">
 													<div class="kt-footer__nav-section">
 														<a href="#">Normativa</a>
 														<a href="#">Estado</a>
 														<a href="#">Soporte</a>
 													</div>
 													<div class="kt-footer__nav-section">
 														<a href="#">Discord</a>
 														<a href="#">Foro</a>
 														<a href="https://www.forwarddevs.com">ForwardDevs</a>
 													</div>
 												</div>
 											</div>
 										</div>
 									</div>
 									<div class="col-lg-4">
 										<div class="kt-footer__section">
 											<h3 class="kt-footer__title">¡Suscríbete a nuestro newsletter!</h3>
 											<div class="kt-footer__content">
 												<form action="" class="kt-footer__subscribe">
 													<div class="input-group">
 														<input type="text" class="form-control" placeholder="Escribe tu e-mail">
 														<div class="input-group-append">
 															<button class="btn btn-brand" type="button">Suscribirme</button>
 														</div>
 													</div>
 												</form>
 											</div>
 										</div>
 									</div>
 								</div>
 							</div>
 						</div>
 						<div class="kt-footer__bottom">
 							<div class="kt-container">
 								<div class="kt-footer__wrapper">
 									<div class="kt-footer__copyright">
 										&copy; 2019 <a href="#"></a>
 									</div>
 									<div class="kt-footer__menu">
 										<font color="white">SAportal &copy; 2019</font><a href="#">Developed by Muphy (ForwardDevs)</a>
 									</div>
 								</div>
 							</div>
 						</div>
 					</div>')

 ?>
