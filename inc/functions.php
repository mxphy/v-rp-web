<?php

function sInput($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function ObtenerFecha()
{
  date_default_timezone_set('UTC');
  return date('d/m/y - H:i:s');
}

function ObtenerHora()
{
  date_default_timezone_set('UTC');
  return date('d/m/y - H');
}

function ObtenerMinuto()
{
  date_default_timezone_set('UTC');
  return date('d/m/y - H:i');
}

function ObtenerIP()
{
  $ip = $_SERVER['REMOTE_ADDR'];
  return $ip;
}

function ObtenerPaisPorIP(){
      $client  = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote  = @$_SERVER['REMOTE_ADDR'];
      $result  = array('country'=>'', 'city'=>'');
      if(filter_var($client, FILTER_VALIDATE_IP)){
          $ip = $client;
      }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
          $ip = $forward;
      }else{
          $ip = $remote;
      }
      $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
      if($ip_data && $ip_data->geoplugin_countryName != null){
          $result['country'] = $ip_data->geoplugin_countryCode;
          $result['city'] = $ip_data->geoplugin_city;
      }
      return $result['country'];
  }

  function RandomString($length = 15) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }

 ?>
