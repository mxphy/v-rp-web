<?php

if (isset($_POST['submit'])) {

  $username = sInput($_POST['username']);
  $pass = sInput($_POST['password']);
  $vcsrf = sInput($_POST['csrf']);

  $csrf = new csrf();
  if ($csrf->validateCsrf($vcsrf)) {
    if (empty($username) || empty($pass)) {
      $msg = "No pueden haber mensajes en blanco";
    }else{
      $password = hash('sha256', $pass);
      $user = new User;

      if ($user->getUser($username, $password)) {
        session_start();
        $_SESSION['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
        $_SESSION['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
        $_SESSION["vrpuser"] = $username;
        header('Location: /');
      }else{
        $msg = "El usuario no existe o la contraseña es inválida";
      }
    }
  }else{
    $msg = "CSRF Token no válido";
  }

}
 ?>
