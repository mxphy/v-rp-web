<?php
use PHPMailer\PHPMailer\PHPMailer;
require_once 'vendor/autoload.php';

if (isset($_POST['submit-forgot'])) {

  $email = sInput($_POST['email']);
  $vcsrf = sInput($_POST['csrf']);

  $csrf = new csrf();
  if ($csrf->validateCsrf($vcsrf)) {
    if (empty($email)) {
      $msg = "No pueden haber mensajes en blanco";
    }else{
      $forgot = new Forgot;

      if ($forgot->checkAccount($email)) {
        $token = RandomString();
        $ipforgot = ObtenerIP();
        $hourforgot = ObtenerHora();

        $forgot->insertToken($token, $email, $ipforgot, $hourforgot);
        $correopass = sprintf('¡Hola!<br>
                               Hemos recibido una solicitud para cambiar tu contraseña, puedes hacerlo fácilmente en el siguiente enlace:<br>
                               <b>Enlace para cambiar la contraseña:</b> <a href="http://www.v-rp.es/forgotpassword.php?token=%s">http://www.v-rp.es/forgotpassword.php?token=%s</a><br>
                               <b>Fecha de expiración:</b> %s:59:00<br><br>
                               Si tienes problemas para cambiar la contraseña, puedes pedir soporte en nuestro <a href="https://discord.gg/pEDwadB">servidor de Discord</a>', $token, $token, $hourforgot);
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 0; // activar debug = 2
        $mail->Host = 'smtp.ionos.es';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;

        $mail-> SMTPOptions = array (
        'ssl' => array (
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
        )
        );
        $mail->Username = "no-reply@muphy.es";

        $mail->Password = "CacaHueteJEJE66667";

        $mail->setFrom('no-reply@v-rp.es', 'V Roleplay (v-rp.es)');

        $mail->addReplyTo('no-reply@v-rp.es', 'V Roleplay (v-rp.es)');

        $mail->addAddress($email, $email);

        $mail->Subject = 'Código de recuperación de contraseña';

        $mail->msgHTML($correopass);

        $mail->CharSet = 'UTF-8';

        $mail->AltBody = $correopass;


        if (!$mail->send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
          echo '';

        }

        function save_mail($mail)
        {
          $path = "{imap.gmail.com:993/imap/ssl}[Gmail]/Sent Mail";
          $imapStream = imap_open($path, $mail->Username, $mail->Password);
          $result = imap_append($imapStream, $path, $mail->getSentMIMEMessage());
          imap_close($imapStream);
          return $result;
        }
        $msgok = "Se ha enviado un correo electrónico a la dirección solicitada con las instrucciones.";

      }else{
        $msg = "No existe ningún usuario con este email";
      }
    }
  }else {
    $msg = "CSRF Token no válido";
  }

}

if (isset($_POST['submit-newpass'])) {
  $token = sInput($_POST['token']);
  $vcsrf = sInput($_POST['csrf']);
  $password = sInput($_POST['pass']);
  $cpass = sInput($_POST['cpass']);

  $csrf = new csrf();
  if ($csrf->validateCsrf($vcsrf)) {
    if (empty($password) || empty($cpass)) {
      $msg = "No pueden haber mensajes en blanco";
    }else{
      if ($password == $cpass){
        $pass = hash('sha256', $password);
        $email = forgot::getEmailByToken($token);
        forgot::changePass($pass, $email, $token);
        header('Location: /?vf=ok');
      }
      else{
        $msg = "Las contraseñas tienen que coincidir";
      }
    }
  }else{
    $msg = "CSRF Token no válido";
  }
}
 ?>
