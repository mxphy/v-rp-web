<?php
require_once 'routes/Router.php';
require_once 'routes/Route.php';
require_once 'routes/RouterC.php';
$router = new Router($_SERVER['REQUEST_URI']);
session_start();
if (!isset($_SESSION['vrpuser'])) {
	$router->add('/', function ()
	{
		include_once 'login.php';
	});
}else{
	$router->add('/', function ()
	{
		include_once 'views/signed.php';
	});
}
$router->add('/vrp', 'RouterC::index');
$router->add('/forgotpassword', 'RouterC::fpass');
// /ruta/con/un/monton/de/parametros
$router->add('/:a/:b/:c/:d/:e/:f', function ($a, $b, $c, $d, $e, $f)
{
	return "$a<br>$b<br>$c<br>$d<br>$e<br>$f";
});
$router->run();
?>
