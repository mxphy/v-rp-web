<?php

class Database extends PDO{

  private $tipodb = "mysql";
  private $servername = "localhost";
  private $user = "root";
  private $password = "";
  private $dbname = "gtalife";

  public function __construct() {
      //Sobreescribo el método constructor de la clase PDO.
      try{
         parent::__construct("{$this->tipodb}:dbname={$this->dbname};host={$this->servername};charset=utf8", $this->user, $this->password);
      }catch(PDOException $e){
         echo 'Ha surgido un error y no se puede conectar a la base de datos. Detalle: ' . $e->getMessage();
         exit;
      }
   }

}

 ?>
