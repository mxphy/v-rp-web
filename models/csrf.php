<?php
class csrf {

  public function getCsrf(){
    if (empty($_SESSION['key'])) {
      $_SESSION['key'] = bin2hex(random_bytes(32));
    }
    $csrf = hash_hmac('sha256', 'dsroemrkcxds', $_SESSION['key']);
    return $csrf;
  }

  public function validateCsrf($csrf){
    if (hash_equals($csrf, $_POST['csrf'])) {
      return true;
    }else{
      return false;
    }
  }

}
?>
