<?php

class Forgot extends Database
{

  public function checkAccount($email){

    $pdo = new Database();

    $query=$pdo->prepare("SELECT * FROM users WHERE Email = :forgotemail");
    $query->bindParam(':forgotemail', $email, PDO::PARAM_STR);
    $query->execute();

    if ($query->rowCount() == 1) {
      return true;
    }

    return false;
  }

  public function insertToken($token, $email, $ipforgot, $hourforgot){

    $pdo = new Database();

    $query=$pdo->prepare("INSERT INTO pcu_password_token (token, email, ip, hour_at) VALUES (:tokenpass, :forgotemail, :ipforgot, :horaforgot)");
    $query->bindParam(':tokenpass', $token, PDO::PARAM_STR);
    $query->bindParam(':forgotemail', $email, PDO::PARAM_STR);
    $query->bindParam(':ipforgot', $ipforgot, PDO::PARAM_STR);
    $query->bindParam(':horaforgot', $hourforgot, PDO::PARAM_STR);
    $query->execute();

  }

  public function checkToken($token){

    $hour = ObtenerHora();

    $pdo = new Database();
    $query=$pdo->prepare("SELECT * FROM pcu_password_token WHERE token = :token and hour_at = :hour");
    $query->bindParam(":token", $token, PDO::PARAM_STR);
    $query->bindParam(":hour", $hour, PDO::PARAM_STR);
    $query->execute();
    if ($query->rowCount() == 1) {
      return true;
    }
    return false;
  }

  public function getEmailByToken($token){

    $pdo = new Database();
    $query=$pdo->prepare("SELECT email FROM pcu_password_token WHERE token = :token");
    $query->bindParam(':token', $token, PDO::PARAM_STR);
    $query->execute();
    $email = $query->fetch();
    return $email;
  }

  public function changePass($pass, $email){

    $pdo = new Database();
    $query=$pdo->prepare("UPDATE users SET Password = :pass WHERE Email = :email");
    $query->bindParam(':pass', $pass, PDO::PARAM_STR);
    $query->bindParam(':email', $email, PDO::PARAM_STR);
    $query->execute();

    $delete=$pdo->prepare ("DELETE FROM pcu_password_token WHERE token = :token");
    $delete->bindParam(':token', $token, PDO::PARAM_STR);
    $delete->execute();

  }

}


 ?>
