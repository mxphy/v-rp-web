<?php

class User extends Database
{

  public function getUser($username, $password){
    $pdo = new Database();
    $query=$pdo->prepare("SELECT * FROM users WHERE Username = :username AND Password = :password");
    $query->bindParam(':username', $username, PDO::PARAM_STR);
    $query->bindParam(':password', $password, PDO::PARAM_STR);
    $query->execute();
    if ($query->rowCount() == 1) {
      return true;
    }
    return false;
  }

  public function getUserInfo($username){
    $pdo = new Database();
    $query=$pdo->prepare("SELECT * FROM users WHERE Username = :username");
    $query->bindParam(':username', $username, PDO::PARAM_INT);
    $query->execute();
    $uInfo = $query->fetchAll();
    return $uInfo;
  }

}


 ?>
